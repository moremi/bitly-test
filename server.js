var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var app = express();
var bodyParser = require('body-parser');
var mongojs = require('mongojs');
var db = mongojs('bitly',['users','sessions','links','tags']);

function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
    	var randomPoz = Math.floor(Math.random() * charSet.length);
    	randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}

function getUser(token,callback){
	db.sessions.findOne({token: token}, function(err,doc){
  	var ans = {
  			error: false,
  			user_id: ''
  		};
  	if (doc){
		ans.user_id=doc.user_id;
		callback(ans);
  	} else {
  		ans.error=true;
  		callback(ans);
  	}
  });
}

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.json());
app.use(cookieParser());

app.post('/auth', function (req,res) {
	console.log(req.body);
	req.body.login=req.body.login.toLowerCase();
	db.users.findOne({login: req.body.login, password: req.body.password}, function (err,doc) {
	//	res.json(doc);
		auth = {
			success: false,			
			token: "",
			error: ""
		};
		if (!doc){
			auth.success=false;
			auth.error="Login or password incorrect!"
		} else {			
			var token = randomString(20);
			db.sessions.insert({user_id: doc._id, token: token});
			auth.success=true;
			auth.token=token;
		}		
		res.json(auth);
	});
});


app.post('/reg', function (req,res) {
	console.log(req.body);
	req.body.login=req.body.login.toLowerCase();
	db.users.findOne({login: req.body.login}, function (err,doc) {
	//	res.json(doc);
		register = {
			success: false,
			error: ""
		};
		if (!doc){
			db.users.insert({login: req.body.login, password: req.body.password}, function(err,doc){
				register.success=true;
				res.json(register);
			});
			
		} else {						
			register.success=false;
			register.error="Login already use!"
			res.json(register);
		}		
		
	});
});

app.get('/userGet', function(req, res) {  
  db.sessions.findOne({token: req.cookies.token}, function(err,doc){
  	var ans = {
  			error: false,
  			login: ''
  		};
  	if (doc){
  		db.users.findOne({_id: doc.user_id},function(err,doc){
  			ans.login=doc.login;
  			res.json(ans);
  		});
  	} else {
  		ans.error=true;
  		res.json(ans);
  	}
  });
});

app.post('/links', function (req,res) {
	console.log(req.body);
	getUser(req.cookies.token,function(user){
		if (user.error==false){
			var link=
			{
				url: req.body.url,
				description: req.body.description,	
				tags: req.body.tags,		
				short: randomString(5),
				user_id: user.user_id,
				counter: 0
			};
			db.links.insert(link, function(err,doc) {
				res.json(doc.short);
				var tags=link.tags.split(',');
				tags.forEach(function(t){
					db.tags.insert({tag: t, link_id: doc._id}, function(err,doc) {
						console.log(doc);
					});				
				});				
			});

		}
	});
});

app.put('/links/:id', function (req,res) {
	var id = req.params.id;
	getUser(req.cookies.token,function(user){
		if (user.error==false){
			db.links.findAndModify({query: {_id: mongojs.ObjectId(id), user_id: user.user_id},
				update: {$set: {description: req.body.description, tags: req.body.tags}},
				new: true}, function (err,doc) {
					res.json(doc);
					if (doc){
						db.tags.remove({link_id: mongojs.ObjectId(id)}, function (err,doc) {
							var tags=req.body.tags.split(',');
							tags.forEach(function(t){
								db.tags.insert({tag: t, link_id: mongojs.ObjectId(id)}, function(err,doc) {

								});				
							});	
						});
					}
				}
			);
		}
	});
});

app.get('/links', function (req,res) {
	getUser(req.cookies.token,function(user){
		if (user.error==false){
			db.links.find({user_id: user.user_id}, function (err,docs) {
				res.json(docs);
			});
		}
	});
});

app.get('/links/:id',function(req,res) {
	var id = req.params.id;
	console.log(id);
	db.links.findOne({_id: mongojs.ObjectId(id)}, function (err,doc) {
		res.json(doc);
	});
});

app.get('/links/tag/:tag',function(req,res) {
	var tag = req.params.tag;
	db.tags.find({tag: tag}, function (err,docs) {
		//res.json(docs);
		var array = [];
		var i=0;
		docs.forEach(function(t){
			db.links.findOne({_id: t.link_id}, function (err,doc) {				
				t=doc;
				doc.user_id="";
				doc.counter="";
				array.push(doc);
				i++;
				if (i==docs.length) {
					res.json(array);
					console.log(array);
				}
			});

		});
				
	});
});


app.delete('/links/:id', function(req,res) {
	var id = req.params.id;
	console.log(id);
	getUser(req.cookies.token,function(user){
		if (user.error==false){
			db.links.remove({_id: mongojs.ObjectId(id), user_id: user.user_id}, function (err,doc) {
				res.json(doc);
				db.tags.remove({link_id: mongojs.ObjectId(doc._id)}, function (err,doc) {});
			});
			
		}
	});
});

app.get('/s/:short',function(req,res) {
	var short = req.params.short;
	db.links.findOne({short: short}, function (err,doc) {
		res.writeHead(200, {"Content-Type": "text/html"});
		res.write('<meta http-equiv="refresh" content="0;URL='+doc.url+'" />Redirect to '+doc.url);
		res.end();
		db.links.findAndModify({query: {short: short},
			update: {$set: {counter: (doc.counter+1)}},
			new: true}, function (err,doc) {
				console.log(doc);
		});
	});
});
app.listen(3000);
console.log("serv run");