function setCookie (name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
	}

var Bitly = angular.module('Bitly',[]);
Bitly.controller('Auth',['$scope','$http', function($scope,$http) {

	$scope.auth = function() {
		$http.post('/auth',$scope.user).success(function(response) {
			console.log(response);
			if (response.success==true) {
				$scope.message="Login success!";
				setCookie("token", response.token, "Mon, 01-Jan-2016 00:00:00 GMT");
				self.location="home.html";
			}
			else { 
				console.log(response.error);
				$scope.message=response.error;				
			};		
		});
	};

	$scope.reg = function() {
		$http.post('/reg',$scope.user).success(function(response) {
			console.log(response);
			if (response.success==true) {
				$scope.message="Registration success! You can Sign in!";				
			} else { 
				$scope.message=response.error;				
			};		
		});
	};

	var userGet = function() {
		$http.get('/userGet').success(function(response) {
			if (response.error==false){
				self.location="/home.html";
			} 			
		});
	};
	userGet();

}]);

Bitly.controller('Home',['$scope','$http', function($scope,$http) {
	var userGet = function() {
		$http.get('/userGet').success(function(response) {
			if (response.error==false){
				$scope.user = "Welcome "+response.login;	
			} else {
				$scope.user = "Please authorize!";
				self.location="/";
			}			
		});
	};
	userGet();

	$scope.logout = function() {
		setCookie("token", "", {
			expires: -1
		});
		self.location="/";
	};

	var refresh = function() {
		$http.get('/links').success(function(response) {
			response.forEach(function(a){
				a.short='http://'+location.host+'/s/'+a.short;
			});
			$scope.linkslist = response;
			$scope.bit = "";
			$scope.whatis="Your links";
		});
	};
	refresh();

	$scope.shortLink = function() {
		$http.post('/links',$scope.bit).success(function(response) {
			console.log(response);
			refresh();
		});
	};
	
	$scope.remove = function (id) {
		$http.delete('/links/'+id).success(function(response) {
			console.log(response);
			refresh();
		});
	};

	$scope.searchtag = function (tag) {
		$http.get('/links/tag/'+tag).success(function(response) {
			response.forEach(function(a){
				a.short='http://'+location.host+'/s/'+a.short;
			});
			$scope.linkslist = response;
			$scope.whatis='Links with tag "'+tag+'"';
			console.log(response);
		});
	};
	var parseQueryString = function (strQuery) {
		var strSearch   = strQuery.substr(1),
		strPattern  = /([^=]+)=([^&]+)&?/ig,
		arrMatch    = strPattern.exec(strSearch),
		objRes      = {};
		while (arrMatch != null) {
			objRes[arrMatch[1]] = arrMatch[2];
			arrMatch = strPattern.exec(strSearch);
		}
		return objRes;
	};
	var queryStr = parseQueryString(window.location.search);
	var tag='';
	if (queryStr.tag!='') 
	{
		tag=queryStr.tag;
		$scope.linkslist = null;
		$scope.whatis='No found links with tag "'+tag+'"';	
		$scope.searchtag(tag);
	}
}]);


Bitly.controller('Info',['$scope','$http', function($scope,$http) {
	var userGet = function() {
		$http.get('/userGet').success(function(response) {
			if (response.error==false){
				$scope.user = "Welcome "+response.login;	
			} else {
				$scope.user = "Please authorize!";
				self.location="/";
			}			
		});
	};
	userGet();

	var parseQueryString = function (strQuery) {
		var strSearch   = strQuery.substr(1),
		strPattern  = /([^=]+)=([^&]+)&?/ig,
		arrMatch    = strPattern.exec(strSearch),
		objRes      = {};
		while (arrMatch != null) {
			objRes[arrMatch[1]] = arrMatch[2];
			arrMatch = strPattern.exec(strSearch);
		}
		return objRes;
	};
	var queryStr = parseQueryString(window.location.search);
	var id='';
	if (queryStr.id!='') id=queryStr.id;

	var tagsUpd = function (){
		$http.get('/links/'+id).success(function(response) {
				response.short='http://'+location.host+'/s/'+response.short;
				$scope.bit=response;
				var tags=response.tags.split(',');
				$scope.tags=tags;
			});
	}
	tagsUpd();
	$scope.edit = function(id) {
		$http.put('/links/'+ $scope.bit._id, $scope.bit).success(function (response) {
			$scope.message="Edited!";
			tagsUpd();
		})
	};
}]);